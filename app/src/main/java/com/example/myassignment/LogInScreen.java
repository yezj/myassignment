package com.example.myassignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * set the button id
 * set the onclicklistener
 * intent to another page
 */
public class LogInScreen extends AppCompatActivity {
    Button letgo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in_screen);
        letgo = (Button) findViewById(R.id.letgo);

        letgo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LogInScreen.this, HomeLocation.class));
            }
        });
    }

    /**
     * get the firebase current user
     * if user login before will skip this page
     */
    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        Log.v("Onstart:", "loginscreen");
        if (user != null) {
            // If user is already logged in upon opening, go to home intent
            Intent intent = new Intent(LogInScreen.this, HomeLocation.class);
            startActivity(intent);
            finish();
        }
    }
}
