package com.example.myassignment;

import android.Manifest;

public class Constant {
    public static final String FINELOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    public static final String COURSELOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    public static final int PERMISSION_CODE = 123;
}
