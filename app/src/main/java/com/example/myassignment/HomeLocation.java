package com.example.myassignment;

import android.content.Intent;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;

//import static com.example.myassignment.R.string.api_key;

public class HomeLocation extends AppCompatActivity {
    Button gpsbtn, nextbtn;
    EditText phoneNumber, secondPoneNumber;
    private String getPlace, getAddress;
    private LatLng latLng;
    private PlacesClient placesClient;
    //Firebase user
    private FirebaseUser currentFireabseUser;
    private DatabaseReference databaseReference;


    /**
     * set the button id
     * set the autoomplete api key
     * set the autocompletesupportfragement
     * initialized the autcomplete
     * set the autocomplete with placeID, Latitude and longitude, Name, Address
     *
     * @param savedInstanceState get the placesName, ID and address
     *                           set the onclicklistener
     *                           convert the data into string
     *                           upload the place information onto firebase
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_location);
//        gpsbtn = (Button) findViewById(R.id.button);
        nextbtn = (Button) findViewById(R.id.button3);
        phoneNumber = (EditText) findViewById(R.id.editText2);
        secondPoneNumber = (EditText) findViewById(R.id.editText6);
        String apiKey = "AIzaSyAOe6TknXHJNTITfnH-NMMKuTr5kzgyjwk";
        final AutocompleteSupportFragment autocompleteSupportFragment =
                (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.autocomplete);

        // databaseReference = FirebaseDatabase.getInstance().getReference("Latlng");

        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), apiKey);
        }

        placesClient = Places.createClient(this);
        autocompleteSupportFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.LAT_LNG, Place.Field.NAME, Place.Field.ADDRESS));
        autocompleteSupportFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                Log.v("Tag", "Place" + place.getName() + "," + place.getId());
                getPlace = place.getName();
                latLng = place.getLatLng();
                getAddress = place.getAddress();
            }

            @Override
            public void onError(@NonNull Status status) {
                Log.d("Tag", "Error");
                Toast.makeText(HomeLocation.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });


        nextbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                double latitude = latLng.latitude;
//                double longtitude = latLng.longitude;
                String number = phoneNumber.getText().toString();
                String secondNumber = secondPoneNumber.getText().toString();

//                int intNumber = Integer.parseInt(number);

                if (number.isEmpty() || secondNumber.isEmpty()) {
                    Toast.makeText(HomeLocation.this, "Do not emepty any field!", Toast.LENGTH_LONG).show();
                } else {
                    currentFireabseUser = FirebaseAuth.getInstance().getCurrentUser();
                    Toast.makeText(HomeLocation.this, "" + currentFireabseUser.getDisplayName() + "\n" + currentFireabseUser.getEmail(), Toast.LENGTH_LONG).show();
                    String userID = currentFireabseUser.getUid();
                    Log.v("address", "" + getAddress);
                    post obj = new post(userID, getPlace, number, getAddress, secondNumber);
                    uploadPost(obj);
                    Intent intent = new Intent(HomeLocation.this, Menu.class);
                    startActivity(intent);
                }
            }


        });


    }

    /**
     * create a upload class
     * declare the path
     *
     * @param obj the upload data
     */
    private void uploadPost(post obj) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase
                .getInstance();
        String userID = FirebaseAuth.getInstance().getUid();
        DatabaseReference databaseReference = firebaseDatabase.getReference().child("User").child(userID).child("Information"); // set the path of firebase
        String ID = databaseReference.getKey();
        //obj.setID(ID);
        DatabaseReference databaseReferenceX = firebaseDatabase.getReference().child("User").child(userID);
        //databaseReferenceX.setValue(ID);


        databaseReference.setValue(obj).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(HomeLocation.this, "Sucess", Toast.LENGTH_SHORT).show();
            }
        });

    }

    /**
     * set the User id
     * declare the firebase path
     * check does the firebase path child has data
     */
    @Override
    protected void onStart() {
        super.onStart();
        currentFireabseUser = FirebaseAuth.getInstance().getCurrentUser();
        String ID = currentFireabseUser.getUid();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("User").child(ID);
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild("Information") && user != null) {
                    Log.v("Onstart:", "homel");
                    Intent intent = new Intent(HomeLocation.this, Menu.class);
                    startActivity(intent);
                    finish();
                } else {

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
