package com.example.myassignment;

public class updateHealth {
    private String sex, sickness, medicine, weight, height, date;


    public updateHealth() {
    }

    /**
     * set the constructor
     *
     * @param sex
     * @param sickness
     * @param medicine
     * @param weight
     * @param height
     * @param date
     */
    public updateHealth(String sex, String sickness, String medicine, String weight, String height, String date) {

        this.sex = sex;
        this.sickness = sickness;
        this.medicine = medicine;
        this.weight = weight;
        this.height = height;
        this.date = date;
    }


    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getSickness() {
        return sickness;
    }

    public void setSickness(String sickness) {
        this.sickness = sickness;
    }

    public String getMedicine() {
        return medicine;
    }

    public void setMedicine(String medicine) {
        this.medicine = medicine;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}


