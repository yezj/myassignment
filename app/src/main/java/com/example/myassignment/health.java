package com.example.myassignment;

import android.content.Intent;
import android.content.res.Configuration;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class health extends AppCompatActivity {
    ImageView profilepic;
    EditText name, sex, weight, height, sickness, date, medicine;
    Button done;
    FirebaseUser firebaseUser;
    DatabaseReference databaseReference;
    String sexText, sicknessText, medicineText, dateText, weightText, heightText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) { // Landscape mode
        } else {
        }

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        profilepic = findViewById(R.id.imageView3);
        name = findViewById(R.id.editText3);
        sex = findViewById(R.id.editText4);
        weight = findViewById(R.id.editText8);
        height = findViewById(R.id.editText9);
        sickness = findViewById(R.id.editText11);
        date = findViewById(R.id.editText5);
        medicine = findViewById(R.id.editText10);
        done = findViewById(R.id.button6);

        Glide.with(this).load(firebaseUser.getPhotoUrl()).into(profilepic);

        String displayName = firebaseUser.getDisplayName();
        name.setText(displayName);


        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sexText = sex.getText().toString();
                medicineText = medicine.getText().toString();
                dateText = date.getText().toString();
                sicknessText = sickness.getText().toString();
                weightText = weight.getText().toString();
                heightText = height.getText().toString();

                if (sexText.isEmpty() || sicknessText.isEmpty() || medicineText.isEmpty()
                        || dateText.isEmpty() || weightText.isEmpty() || heightText.isEmpty()) {


                    Toast.makeText(health.this, "Do not empty the field", Toast.LENGTH_LONG).show();

                } else {

                    updateHealth obj = new updateHealth(sexText, sicknessText, medicineText, weightText, heightText, dateText);
                    Log.v("helloyap", "" + obj);
                    uploadPost(obj);
                    Intent intent = new Intent(health.this, Menu.class);
                    startActivity(intent);

                }


            }
        });
    }


    /**
     * create a upload class
     * declare the path
     *
     * @param obj the upload data
     */

    private void uploadPost(updateHealth obj) { //upload method
        FirebaseDatabase firebaseDatabase = FirebaseDatabase
                .getInstance();
        String userID = FirebaseAuth.getInstance().getUid();
        DatabaseReference databaseReference = firebaseDatabase.getReference().child("User").child(userID).child("Health");

        databaseReference.setValue(obj).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(health.this, "Sucess", Toast.LENGTH_SHORT).show();
            }
        });

    }


    /**
     * get user id
     * declare the firebase path
     * check the path child has data
     */
    @Override
    protected void onStart() {
        super.onStart();

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        String ID = firebaseUser.getUid();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("User").child(ID);
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild("Health")) {
//                    Log.v("Onstart:", "homel");

                    if (!dataSnapshot.hasChild("Information")) {
                        Intent intent = new Intent(health.this, HomeLocation.class);
                        startActivity(intent);
                        finish();
                    } else {
                        showHealth();

                    }
                } else {

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    /**
     * get the user id
     * set the upload path
     * convert the data to string
     */
    private void showHealth() {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        String ID = firebaseUser.getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("User").child(ID).child("Health");

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String sexS = dataSnapshot.child("sex").getValue().toString();
                String heightS = dataSnapshot.child("height").getValue().toString();
                String medicineS = dataSnapshot.child("medicine").getValue().toString();
                String sicknessS = dataSnapshot.child("sickness").getValue().toString();
                String weightS = dataSnapshot.child("weight").getValue().toString();
                String dateS = dataSnapshot.child("date").getValue().toString();

                height.setText(heightS);
                sex.setText(sexS);
                weight.setText(weightS);
                date.setText(dateS);
                sickness.setText(sicknessS);
                medicine.setText(medicineS);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}