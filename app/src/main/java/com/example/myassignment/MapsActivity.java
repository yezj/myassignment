package com.example.myassignment;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.data.model.User;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

import static com.example.myassignment.Constant.COURSELOCATION;
import static com.example.myassignment.Constant.FINELOCATION;
import static com.example.myassignment.Constant.PERMISSION_CODE;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    private FusedLocationProviderClient fusedLocationProviderClient;
    private GoogleMap mMap;
    boolean locationGranted = false;
    private double currentLatitude, currentLongtitude;
    Button sendLocation;
    private FirebaseUser currentFirebaseUser;
    private final static int PERMISSIONS_REQUEST_SMS = 0;
    private final static int CALLING_CODE = 111;
    private DatabaseReference databaseReference;
    private String number, secondNumber;
    private String message;
    MediaPlayer mediaPlayer;
    Dialog dialog;
    String[] SMSPERMISSION = {Manifest.permission.SEND_SMS, Manifest.permission.READ_PHONE_STATE};
    int i = 0;


    @Override
    protected void onStart() {
        super.onStart();
        isPermissionGranted();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        sendLocation = (Button) findViewById(R.id.sendLocation);
        currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        String userID = currentFirebaseUser.getUid();
        callingPermission();

//        SmsManager smsManager = SmsManager.getDefault();
//        smsManager.sendTextMessage("phoneNo", null, "sms message", null, null);
        sendLocation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                isPermissionGranted();
                i++;
//                Handler handler = new Handler();

                Runnable runnable = new Runnable() {

                    @Override
                    public void run() {
                        i = 0;

                    }
                };

                Log.v("one", "" + i);

                if (i == 1) {
                    currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                    String userID = currentFirebaseUser.getUid();
                    createObject();
                    databaseReference = FirebaseDatabase.getInstance().getReference().child("User").child(userID).child("Information");

                    databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            number = dataSnapshot.child("phoneNumber").getValue().toString();
                            secondNumber = dataSnapshot.child("secondPhoneNumber").getValue().toString();
                            Log.v("Number", number);
                            sendSMS(number, secondNumber);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                } else if (i == 2) {
                    alarm();
                    i = 0;
                    Toast.makeText(MapsActivity.this, "Alarm", Toast.LENGTH_LONG).show();
                    AlertDialog dialog = new AlertDialog.Builder(MapsActivity.this).setTitle("Stop the alarm").setPositiveButton("Stop", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            stopMedia();
                        }
                    }).setNegativeButton("Emergency calling", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(MapsActivity.this, "Calling", Toast.LENGTH_SHORT).show();
                            stopMedia();
                            Intent intent = new Intent(Intent.ACTION_CALL);
                            intent.setData(Uri.parse("tel:911"));
                            startActivity(intent);

                        }
                    }).show();
                }
            }
        });

    }

    /**
     * stop the mp3 function
     */
    public void stopMedia() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }

    }

    /**
     * declare the MP3 file location
     * play the MP3 file
     * set the loop
     */
    public void alarm() {
        mediaPlayer = MediaPlayer.create(this, R.raw.alarm);
        mediaPlayer.start();
        mediaPlayer.setLooping(true);

    }


    private void createObject() {
        currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        String userID = currentFirebaseUser.getUid();
        Log.v("Lat", "" + currentLatitude);
        Log.v("Long", "" + currentLongtitude);
        lastknown obj = new lastknown(userID, currentLatitude, currentLongtitude);
        uploadpost(obj);
    }

    private void sendSMS(String number, String secondNumber) {// method for asking the SMS permission and READ phone state
        String googleLink = "https://maps.google.com/?q=" + currentLatitude + "," + currentLongtitude;
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.SEND_SMS)) {
                ActivityCompat.requestPermissions(this, SMSPERMISSION, PERMISSION_CODE); // if not allowed, ask user for permission
            } else {
                ActivityCompat.requestPermissions(this, SMSPERMISSION, PERMISSION_CODE); // if not allowed, ask user for permission
            }
        } else {
            message = "My current location +\n" + "Latitude:" + currentLatitude + "\n" + "Longtitude:" + currentLongtitude + "\n Google Map link"
                    + "\n" + googleLink; //default sms content
            SmsManager smsManager = SmsManager.getDefault(); // initialise the sms manager API
            smsManager.sendTextMessage(number, null, message, null, null);
            smsManager.sendTextMessage(secondNumber, null, message, null, null);// declare the phone number and message content
            Toast.makeText(getApplicationContext(), "SMS sent.", Toast.LENGTH_LONG).show();// toast for showing the message is success sent
        }
    }


    private void uploadpost(lastknown obj) {
        String userID = FirebaseAuth.getInstance().getUid(); //get the firebase user account ID
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference().child("User").child(userID).child("CurrentLocation");//  declare the path of database


        databaseReference.setValue(obj).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) { //put the parameter of the obj class
                Toast.makeText(MapsActivity.this, "Updated", Toast.LENGTH_LONG).show(); //show the toast when the update is success
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Toast.makeText(this, "The maps is ready to used", Toast.LENGTH_SHORT).show();

        getLocation();
        mMap.setMyLocationEnabled(true);


    }

    private void isPermissionGranted() {
        Log.d("Tag", "Get location permission");
        String[] permission = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}; //Array for store the 2 permissions
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(), FINELOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this.getApplicationContext(), COURSELOCATION) == PackageManager.PERMISSION_GRANTED) {
                locationGranted = true;
                getLocation();
            } else {
                ActivityCompat.requestPermissions(this, permission, PERMISSION_CODE);
            }
        } else {
            ActivityCompat.requestPermissions(this, permission, PERMISSION_CODE);
        }
    }

    private void getLocation() {
        Log.d("Tag", "Get location");
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        if (locationGranted) {
            Log.d("Tag", "Get location gramted");
            final Task location = fusedLocationProviderClient
                    .getLastLocation();

            location.addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()) {
                        Log.d("Tag", "Location is found");
                        Location currentLocation = (Location) task.getResult();
                        currentLatitude = currentLocation.getLatitude();
                        currentLongtitude = currentLocation.getLongitude();
                        Log.v("GetLLat", "" + currentLatitude);
                        Log.v("GetLLong", "" + currentLongtitude);
                        createObject();
                        LatLng mapcamera = new LatLng(currentLatitude, currentLongtitude);
                        Log.v("getcurrentlong", "" + currentLatitude);
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(mapcamera));


                    } else {
                        Log.d("Tag", "Location is null");
                        Toast.makeText(MapsActivity.this, "Location Failed", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        stopMedia();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        message = "My current location +\n" + "Latitude:" + currentLatitude + "\n" + "Longtitude:" + currentLongtitude;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_SMS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(number, null, message, null, null);
                    Toast.makeText(getApplicationContext(), "SMS sent.",
                            Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "SMS faild, please try again.", Toast.LENGTH_LONG).show();
                    return;
                }
            }
        }
    }

    private void callingPermission() {
        final String[] permission = {Manifest.permission.CALL_PHONE}; //Array for store the 2 permissions
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(MapsActivity.this, permission, CALLING_CODE);
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MapsActivity.this, Manifest.permission.CALL_PHONE)) {
                AlertDialog dialog = new AlertDialog.Builder(this).setTitle("Please accept the permission").setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(MapsActivity.this, permission, CALLING_CODE);
                    }
                }).setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MapsActivity.this, "User deny", Toast.LENGTH_SHORT).show();
                    }
                }).show();
            }

        }
    }
}