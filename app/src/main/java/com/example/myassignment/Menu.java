package com.example.myassignment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static com.example.myassignment.Constant.COURSELOCATION;
import static com.example.myassignment.Constant.FINELOCATION;
import static com.example.myassignment.Constant.PERMISSION_CODE;


public class Menu extends AppCompatActivity {
    private DrawerLayout drawerLayout;
    private Boolean locationGranted = false;
    ActionBarDrawerToggle toggle;
    ImageView profilepic, menuPic;
    String[] SMSPERMISSION = {Manifest.permission.SEND_SMS, Manifest.permission.READ_PHONE_STATE};
    TextView getName, getPlace;
    DatabaseReference databaseReference;
    FirebaseUser currentFirebaseUser;
    String userID, userName;
    Button location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_layout);


        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) { // Landscape mode
        } else {
        }

        getName = (TextView) findViewById(R.id.textView12);
//        getPlace = (TextView) findViewById(R.id.textView11);
        location = (Button) findViewById(R.id.button4);
        menuPic = (ImageView) findViewById(R.id.imageView6);

        Toolbar toolbar = findViewById(R.id.toolbar); //set the toolbar for put the Hamburger
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.drawer_layout);
        isPermissionGranted();
        sendSMS();

        currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        userID = currentFirebaseUser.getUid();
        userName = currentFirebaseUser.getDisplayName();
        getName.setText(userName);

        Log.v("getphoto", "" + currentFirebaseUser.getPhotoUrl());


        databaseReference = FirebaseDatabase.getInstance().getReference().child("User").child(userID).child("Information");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//               String pName = dataSnapshot.child("placeName").getValue().toString();
//               getPlace.setText(pName);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close); //hamburger icon
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);

        View headerView = navigationView.getHeaderView(0);

        profilepic = headerView.findViewById(R.id.profile); // user profile pic\

        Glide.with(this).load(currentFirebaseUser.getPhotoUrl()).into(profilepic);
        Glide.with(this).load(currentFirebaseUser.getPhotoUrl()).into(menuPic);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();
                if (id == R.id.drawer_location) {
                    Intent intent = new Intent(Menu.this, MapsActivity.class);
                    startActivity(intent);

                } else if (id == R.id.drawer_contact) {
                    Intent intent = new Intent(Menu.this, Contact.class);
                    startActivity(intent);


                } else if (id == R.id.signOut) {
                    FirebaseAuth.getInstance().signOut();
                    Intent intent = new Intent(Menu.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else if (id == R.id.health) {
                    Intent intent = new Intent(Menu.this, health.class);
                    startActivity(intent);
                }


                return false;
            }
        });

        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Menu.this, MapsActivity.class);
                startActivity(intent);

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
//            super.onBackPressed();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }

    private void isPermissionGranted() { // check the permission
        Log.d("Tag", "Get location permission");
        String[] permission = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(), FINELOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this.getApplicationContext(), COURSELOCATION) == PackageManager.PERMISSION_GRANTED) {
                locationGranted = true;

            } else {
                ActivityCompat.requestPermissions(this, permission, PERMISSION_CODE);
            }
        } else {
            ActivityCompat.requestPermissions(this, permission, PERMISSION_CODE);
        }
    }

    private void sendSMS() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.SEND_SMS)) {

            } else {
                ActivityCompat.requestPermissions(this, SMSPERMISSION, PERMISSION_CODE);
            }
        }
    }


}