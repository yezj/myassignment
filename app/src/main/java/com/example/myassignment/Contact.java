package com.example.myassignment;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class Contact extends AppCompatActivity {
    TextView viewPhone, viewPhone2;
    EditText phoneNumber, phoneNumber2;
    Button Enter;
    DatabaseReference databaseReference, databaseReferenceX;
    FirebaseUser currentFirebaseUser;
    String number, userID, numberx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) { // Landscape mode
        } else {
        }

        viewPhone = (TextView) findViewById(R.id.textView5);
        phoneNumber = (EditText) findViewById(R.id.editText);
        phoneNumber2 = (EditText) findViewById(R.id.editText7);
        viewPhone2 = (TextView) findViewById(R.id.textView10);
        Enter = findViewById(R.id.button2);


        /** declare the firebase path first
         * add on the datachange to get the value
         */


        currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        userID = currentFirebaseUser.getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("User").child(userID).child("Information").child("phoneNumber");
        databaseReferenceX = FirebaseDatabase.getInstance().getReference().child("User").child(userID).child("Information").child("secondPhoneNumber");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                number = dataSnapshot.getValue().toString();
                numberx = dataSnapshot.getValue().toString();
                viewPhone.setText(number);
                viewPhone2.setText(numberx);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        /** add the oncllicklistener
         * overwrite the data on firebase
         */
        Enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String getNumber = phoneNumber.getText().toString();
                final String getNumber2 = phoneNumber2.getText().toString();
                databaseReference.setValue(getNumber);
                databaseReferenceX.setValue(getNumber2);

                Intent intent = new Intent(Contact.this, Menu.class);
                startActivity(intent);
                Toast.makeText(Contact.this, "Sucess change phone number!.", Toast.LENGTH_LONG).show();

            }
        });
    }

}
