package com.example.myassignment;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.storage.StorageManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.firebase.ui.auth.data.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class profilePicture extends AppCompatActivity {


    Button upload;
    Uri uri;
    ImageView selectImg;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    final static int PERMISSIONCODE = 123;
    final static int REQUESTCODE = 123;
    String[] readPermission = {Manifest.permission.READ_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_picture);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) { // Landscape mode
        } else {
        }
        upload = findViewById(R.id.button5);
        selectImg = findViewById(R.id.imageView5);

        firebaseAuth = FirebaseAuth.getInstance();

        selectImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 22) {
                    permission();
                } else {
                    openGallery();
                }
            }
        });


        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (uri == null) {
                    firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                    String ID = firebaseUser.getUid();
                    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("User").child(ID);
                    final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    databaseReference.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.hasChild("Health")) {
//                    Log.v("Onstart:", "homel");
                                if (!dataSnapshot.hasChild("Information")) {
                                    Intent intent = new Intent(profilePicture.this, Menu.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Intent intent = new Intent(profilePicture.this, HomeLocation.class);
                                    startActivity(intent);
                                }
                            } else {
                                Intent intent = new Intent(profilePicture.this, health.class);
                                startActivity(intent);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                } else {
                    updateProfile(uri, firebaseAuth.getCurrentUser());
                    Toast.makeText(profilePicture.this, "secuss upload", Toast.LENGTH_LONG
                    ).show();
                }

            }
        });
    }

    private void updateProfile(Uri imgage, final FirebaseUser firebaseUser) {

        //update the photo to firebase and get URI
        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("userPhoto");// declare the storage reference
        final StorageReference path = storageReference.child(imgage.getLastPathSegment()); //declare the storage reference to the image ID
        path.putFile(imgage).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() { // match the image to its ID, UploadTask is for monitor the upload status
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                path.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) { //get the download URI link

                        UserProfileChangeRequest userProfileChangeRequest = new UserProfileChangeRequest.Builder() //allow the user upload their photo
                                .setPhotoUri(uri)
                                .build();

                        firebaseUser.updateProfile(userProfileChangeRequest).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Log.v("photo", "" + firebaseUser.getPhotoUrl());
                                    Intent intent = new Intent(profilePicture.this, health.class);
                                    startActivity(intent);
                                }
                            }
                        });
                    }
                });
            }
        });
    }

    private void permission() {
        if (ContextCompat.checkSelfPermission(profilePicture.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {// check is the permission granted
            if (ActivityCompat.shouldShowRequestPermissionRationale(profilePicture.this, Manifest.permission.READ_EXTERNAL_STORAGE)) { //if user deny the permission
                Toast.makeText(profilePicture.this, "please accept the permission", Toast.LENGTH_LONG).show();
                AlertDialog dialog = new AlertDialog.Builder(this).setTitle("Please accept the permission").setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(profilePicture.this,
                                readPermission, PERMISSIONCODE);
                    }
                }).setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(profilePicture.this, "User deny", Toast.LENGTH_SHORT).show();
                    }
                }).show();
            } else { //if the user haven't set the permission yet
                ActivityCompat.requestPermissions(this, readPermission, PERMISSIONCODE);
            }
        } else {
            openGallery();
        }

    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT); //open outside the application
        intent.setType("image/*"); // open the gallery
        startActivityForResult(intent, REQUESTCODE); // start the new intent, use the startActivityForResult for passing the data from another activity to current activity
    }


    @Override

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == REQUESTCODE && data != null) { // if the user choose photo and the result will return it RESULT_OK ,
            // requestCode is the code sent by the method, use the intent data to pass the data
            uri = data.getData(); // the selected photo will become an URI
            selectImg.setImageURI(uri); // put the URL LINK to the image view
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        String ID = firebaseUser.getUid();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("User").child(ID);
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (firebaseUser.getPhotoUrl() == null) {

        } else {
            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.hasChild("Health")) {
//                    Log.v("Onstart:", "homel");

                        if (!dataSnapshot.hasChild("Information")) {
                            Intent intent = new Intent(profilePicture.this, HomeLocation.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(profilePicture.this, Menu.class);
                            startActivity(intent);
                            finish();

                        }
                    } else {
                        Intent intent = new Intent(profilePicture.this, health.class);
                        startActivity(intent);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }


    }

}
