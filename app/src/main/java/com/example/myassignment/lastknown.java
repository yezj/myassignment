package com.example.myassignment;

public class lastknown {
    private double wayLatitude, wayLongtitude;
    private String userID;

    public lastknown() {
    }

    public lastknown(String userID, double wayLatitude, double wayLongtitude) {
        this.wayLatitude = wayLatitude;
        this.wayLongtitude = wayLongtitude;
        this.userID = userID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public double getWayLatitude() {
        return wayLatitude;
    }

    public void setWayLatitude(double wayLatitude) {
        this.wayLatitude = wayLatitude;
    }

    public double getWayLongtitude() {
        return wayLongtitude;
    }

    public void setWayLongtitude(double wayLongtitude) {
        this.wayLongtitude = wayLongtitude;
    }
}

