package com.example.myassignment;

/**
 * declare the variables
 */
public class post {

    private double latitude, longtitude;
    private String placeName;
    private String userid;
    private String phoneNumber, secondPhoneNumber;
    private String address;

    public post() {
    }


    /**
     * set the constructor
     *
     * @param userid
     * @param placeName
     * @param phoneNumber
     * @param address
     * @param secondPhoneNumber
     */
    public post(String userid, String placeName, String phoneNumber, String address, String secondPhoneNumber) {
        this.latitude = latitude;
        this.longtitude = longtitude;
        this.placeName = placeName;
        this.userid = userid;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.secondPhoneNumber = secondPhoneNumber;
    }

    public String getSecondPhoneNumber() {
        return secondPhoneNumber;
    }

    public void setSecondPhoneNumber(String secondPhoneNumber) {
        this.secondPhoneNumber = secondPhoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }
}
