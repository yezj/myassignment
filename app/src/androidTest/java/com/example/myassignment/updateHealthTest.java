package com.example.myassignment;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class updateHealthTest {
    updateHealth updateHealth;
    private String sex = "male", sickness = "asthma", medicine = "seafood",  weight = "62", height = "177", date = "98/07/27";


    @Before
    public void setUp() throws Exception {
        updateHealth = new updateHealth();
    }

    @Test
    public void testConstructors() {
        assertNotNull("Could not create basic post object. ", updateHealth);
        updateHealth updateHealth = new updateHealth (sex, sickness, medicine, weight, height, date);
        assertNotNull("Could not create complex post object. ", updateHealth);

        assertEquals("Name is not set as expected. ", sex, updateHealth.getSex());
    }

    @Test
    public void getSex() {
        updateHealth.setSex(sex);
        assertEquals("sex is not set as expected. ", sex, updateHealth.getSex());
    }

    @Test
    public void getSickness() {
        updateHealth.setSickness(sickness);
        assertEquals("sickness is not set as expected. ", sickness, updateHealth.getSickness());
    }

    @Test
    public void getMedicine() {
        updateHealth.setMedicine(medicine);
        assertEquals("medicine is not set as expected. ", medicine, updateHealth.getMedicine());
    }

    @Test
    public void getWeight() {
        updateHealth.setWeight(weight);
        assertEquals("weight is not set as expected. ", weight, updateHealth.getWeight());
    }

    @Test
    public void getHeight() {
        updateHealth.setHeight(height);
        assertEquals("height is not set as expected. ", height, updateHealth.getHeight());
    }

    @Test
    public void getDate() {
        updateHealth.setDate(date);
        assertEquals("date is not set as expected. ", date, updateHealth.getDate());
    }
}