package com.example.myassignment;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class postTest {


    post post;
    private double latitude =0.0, longtitude = 0.0;
    private String placeName = "Singapore";
    private String userid = "213124dd";
    private String phoneNumber = "324581611", secondPhoneNumber = "89373822";
    private String address = "road maple";

    @Before
    public void setUp() throws Exception {
        post = new post();
    }

    @Test
    public void testConstructors() {
        assertNotNull("Could not create basic post object. ", post);
        post post = new post(userid, placeName, phoneNumber, address, secondPhoneNumber);
        assertNotNull("Could not create complex post object. ", post);

        assertEquals("Name is not set as expected. ", userid, post.getUserid());
    }


    @org.junit.Test
    public void getSecondPhoneNumber() {
        post.setSecondPhoneNumber(secondPhoneNumber);
        assertEquals("secondPhoneNumber is not set as expected. ", secondPhoneNumber, post.getSecondPhoneNumber());
    }


    @org.junit.Test
    public void getAddress() {
        post.setAddress(address);
        assertEquals("address is not set as expected. ", address, post.getAddress());
    }



    @org.junit.Test
    public void getPhoneNumber() {
        post.setPhoneNumber(phoneNumber);
        assertEquals("phoneNumber is not set as expected. ", phoneNumber, post.getPhoneNumber());
    }





    @org.junit.Test
    public void getLatitude() {
        post.setLatitude(latitude);
        assertEquals("latitude is not set as expected. ", latitude, post.getLatitude(), 0);

    }




    @org.junit.Test
    public void getLongtitude() {
        post.setLatitude(longtitude);
        assertEquals("longtitude is not set as expected. ", longtitude, post.getLongtitude(),0);
    }


    @org.junit.Test
    public void getUserid() {
        post.setUserid(userid);
        assertEquals("userid is not set as expected. ", userid, post.getUserid());
    }


    @org.junit.Test
    public void getPlaceName() {
        post.setPlaceName(placeName);
        assertEquals("placeName is not set as expected. ", placeName, post.getPlaceName());
    }



}